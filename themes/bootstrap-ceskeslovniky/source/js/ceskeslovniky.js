const checkWords = function() {
    const wordInput = document.querySelector("#wordInput");
    const resultsTable = document.querySelector("#resultsTable");
    resultsTable.classList.add("resultsHidden");
    const resultsNote = document.querySelector("#resultsNote");
    resultsNote.classList.add("resultsHidden");

    const words = wordInput.value ? wordInput.value : wordInput.placeholder;
    fetch("https://ceskeslovniky.cz/hunspell/check?words=" + words)
    .then(response => response.json())
    .then(checkedWords => {
        const tableBody = resultsTable.querySelector("tbody");
        tableBody.innerHTML = "";
        const trElement = document.createElement("tr");
        const tdElement = document.createElement("td");
        for (let w = 0; w < checkedWords.length; w++) {
            let currentTr = tableBody.appendChild(trElement.cloneNode());
            for (let col = 0; col < 3; col++) {
                currentTr.appendChild(tdElement.cloneNode());
            }
            const wordTd = currentTr.children[0];
            wordTd.innerText = checkedWords[w].word;
            if (!checkedWords[w].correct) {
                currentTr.children[1].innerText = "není";
                wordTd.classList.remove("isInDictionary");
                wordTd.classList.add("isNotInDictionary");
                currentTr.children[2].innerText = checkedWords[w].suggestions.join(", ");
            }
            else {
                currentTr.children[1].innerText = "je";
                wordTd.classList.add("isInDictionary");
                wordTd.classList.remove("isNotInDictionary");
            }
        }
        resultsTable.classList.remove("resultsHidden");
        resultsNote.classList.remove("resultsHidden");
    })
    .catch((error) => { console.error(error); })
}

document.querySelector("#checkButton").addEventListener("click", function(e) {
    checkWords();
});

document.querySelector("#wordInput").addEventListener("keydown", function(e) {
    if (e.key === "Enter") {
        checkWords();
    }
});