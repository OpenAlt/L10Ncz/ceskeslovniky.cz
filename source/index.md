---
title: Kontrola slov
date: 2019-03-21 12:42:36
---
{% raw %}
<div class="container">
  <div class="row">
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text">Slovo</span>
      </div>
      <input id="wordInput" type="text" class="form-control" placeholder="mihotavý" aria-label="mihotavý" aria-describedby="basic-addon1">
    </div>
  </div>
  <div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2">
      <button id="checkButton" type="button" class="btn btn-primary">Zkontrolovat</button>
    </div>
    <div class="col-md-5"></div>
    </div>
    <table id="resultsTable" class="table table-bordered resultsHidden">
      <thead>
        <tr>
          <th scope="col">Slovo</th>
          <th scope="col">Ve slovníku</th>
          <th scope="col">Návrhy na opravu</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
</div>
<p id="resultsNote" class="resultsHidden">Schází ve slovníku správné slovo, nebo jste v něm naopak nalezli chybné? <a href="about.html#jak-slovnik-vylepsit">Slovník můžete vlastnoručně vylepšit!</a></p>
{% endraw %}
