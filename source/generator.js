'use strict';

class GeneratorWizard {
  constructor(wizardForm, generators, wordCategorySelectLabelText, patternSelectLabelText, submitButtonText) {
    this.wizardForm = wizardForm;
    this.wizardFormSubmitButton = undefined;
    this.generators = generators;
    this.wordCategorySelectLabelText = wordCategorySelectLabelText;
    this.patternSelectLabelText = patternSelectLabelText;
    this.submitButtonText = submitButtonText;
  }

  init() {
    // prepare for wizard form submission
    this.wizardForm.addEventListener('submit', (e) => this.wizardFormSubmitted(e));
    this.wizardFormSubmitButton = document.createElement('input');
    this.wizardFormSubmitButton.type = 'submit';
    this.wizardFormSubmitButton.disabled = true;
    this.wizardFormSubmitButton.style.display = 'none';
    this.wizardFormSubmitButton.value = this.submitButtonText;

    const step1 = document.createElement('div');
    step1.id = 'step1';

    // create word category select
    const wordCategorySelect = document.createElement('select');
    wordCategorySelect.id = 'wordCategory';
    const defaultOption = document.createElement('option');
    wordCategorySelect.appendChild(defaultOption);
    Object.entries(this.generators).forEach((entry) => {
      const [key, generator] = entry;
      const option = document.createElement('option');
      option.value = key;
      option.innerText = generator.label;
      wordCategorySelect.appendChild(option);
    });
    wordCategorySelect.addEventListener('change', () => this.wordCategorySelected());

    // create word category label
    const wordCategorySelectLabel = document.createElement('label');
    wordCategorySelectLabel.htmlFor = wordCategorySelect.id;
    wordCategorySelectLabel.textContent = this.wordCategorySelectLabelText;

    // add word category to wizard form
    step1.appendChild(wordCategorySelectLabel);
    step1.appendChild(this.createNbsp());
    step1.appendChild(wordCategorySelect);
    this.wizardForm.appendChild(step1);
  }

  createNbsp() {
    const span = document.createElement('span');
    span.textContent = String.fromCharCode(160); // non-breakable space
    return span;
  }

  getSelectedWordCategory() {
    const wordCategorySelect = this.wizardForm.querySelector('select#wordCategory');
    if (wordCategorySelect && wordCategorySelect.value !== '') {
      return wordCategorySelect.value;
    } else {
      return null;
    }
  }

  getSelectedPattern() {
    const patternSelect = this.wizardForm.querySelector('select#pattern');
    if (patternSelect && patternSelect.value !== '') {
      return patternSelect.value;
    } else {
      return null;
    }
  }

  getBasicWordForm() {
    const basicWordFormInput = this.wizardForm.querySelector('input#basicWordForm');
    if (basicWordFormInput && basicWordFormInput.value !== '' && basicWordFormInput.value.trim() !== '') {
      return basicWordFormInput.value.trim();
    } else {
      return null;
    }
  }

  wordCategorySelected() {
    const wordCategory = this.getSelectedWordCategory();

    // reset this step
    let step2 = this.wizardForm.querySelector('#step2');
    if (step2) {
      step2.childNodes.forEach((it) => it.remove());
      step2.remove();
    }
    // trigger next steps reset
    this.patternSelected();

    if (!wordCategory) {
      // nothing to show
      return;
    }

    step2 = document.createElement('div');
    step2.id = 'step2';

    // create pattern select
    const patternSelect = document.createElement('select');
    patternSelect.id = 'pattern';
    patternSelect.addEventListener('change', () => this.patternSelected());
    const defaultOption = document.createElement('option');
    patternSelect.appendChild(defaultOption);
    Object.keys(this.generators[wordCategory].patterns).forEach(key => {
      const option = document.createElement('option');
      option.value = key;
      option.innerText = key;
      patternSelect.appendChild(option);
    });

    // create pattern label
    const patternSelectLabel = document.createElement('label');
    patternSelectLabel.htmlFor = patternSelect.id;
    patternSelectLabel.textContent = this.patternSelectLabelText;

    // add pattern select to wizard form
    step2.appendChild(patternSelectLabel);
    step2.appendChild(this.createNbsp());
    step2.appendChild(patternSelect);
    this.wizardForm.appendChild(step2);
  }

  patternSelected() {
    const pattern = this.getSelectedPattern();

    // reset this step
    let step3 = this.wizardForm.querySelector('#step3');
    if (step3) {
      step3.childNodes.forEach((it) => it.remove());
      step3.remove();
    }
    // trigger next steps reset
    this.basicWordFormInputUpdated();

    if (!pattern) {
      // nothing to show
      return;
    }

    step3 = document.createElement('div');
    step3.id = 'step3';

    // create basic word form input
    const basicWordFormInput = document.createElement('input');
    basicWordFormInput.id = 'basicWordForm';
    basicWordFormInput.addEventListener('keyup', () => this.basicWordFormInputUpdated());

    // create basic word form label
    const basicWordFormInputLabel = document.createElement('label');
    basicWordFormInputLabel.htmlFor = basicWordFormInput.id;
    basicWordFormInputLabel.textContent = this.generators[this.getSelectedWordCategory()].input_fields;

    // add basic word form input and label and form submit button
    step3.appendChild(basicWordFormInputLabel);
    step3.appendChild(this.createNbsp());
    step3.appendChild(basicWordFormInput);
    step3.appendChild(document.createElement('br'));
    step3.appendChild(document.createElement('br'));
    step3.appendChild(this.wizardFormSubmitButton);
    this.wizardForm.appendChild(step3);
  }

  basicWordFormInputUpdated() {
    const basicWordForm = this.getBasicWordForm();
    if (basicWordForm) {
      this.wizardFormSubmitButton.disabled = false;
      this.wizardFormSubmitButton.style.display = '';
    } else {
      this.wizardFormSubmitButton.disabled = true;
      this.wizardFormSubmitButton.style.display = 'none';
    }
  }

  wizardFormSubmitted(event) {
    event.preventDefault();
    event.stopPropagation();

    const wordCategory = this.getSelectedWordCategory();
    const pattern = this.getSelectedPattern();
    const basicWordForm = this.getBasicWordForm();

    if (wordCategory && pattern && basicWordForm) {
      this.generateLexemeFormAndSubmit(wordCategory, pattern, basicWordForm);
      console.log(event);
    } else {
      console.log('Form is incomplete!');
    }
  }

  generateLexemeFormAndSubmit(wordCategory, pattern, basicWordForm) {
    const lexemeForm = document.createElement('form');
    lexemeForm.style.display = 'none';
    lexemeForm.action = `https://tools.wmflabs.org/lexeme-forms/template/${this.getSelectedWordCategory()}`;
    lexemeForm.method = 'post';
    this.generators[wordCategory].forms[pattern].forEach((pat, idx) => {
      const patParts = pat.split(' ');
      if (patParts[0] === 's') {
        for (let i = 1; i < patParts.length; i += 3) {
          if (pattern.match(new RegExp(patParts[i+2]))) {
            const suffixToReplace = basicWordForm.slice(basicWordForm.length-parseInt(patParts[i]));
            const newSuffix = patParts[i+1];
            const input = document.createElement('input');
            input.type = 'hidden';
            input.id = `pattern-${idx+1}`;
            input.value = basicWordForm.replace(new RegExp(`${suffixToReplace}$`), newSuffix);
            lexemeForm.appendChild(input);
          }
        }
      }
    });
    const patternRegex = /^(\D*)\/(\d+)(\D*)$/;
    this.generators[wordCategory].patterns[pattern]
      .map((p) => p.match(patternRegex))
      .forEach((match) => {
        const input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'form_representation';
        if (match) {
          input.value = `${match[1]}${lexemeForm.querySelector(`#pattern-${match[2]}`).value}${match[3]}`;
        } else {
          input.value = '';
        }
        lexemeForm.appendChild(input);
      });
    document.body.appendChild(lexemeForm);
    lexemeForm.submit();
  }
}

const generators = {
  'czech-adjective': {
    label: 'česká přídavná jména',
    input_fields: [
      'Zadejte mužský rod, jednotné číslo, 1. pád 1. stupně přídavného jména ("mladý", "jarní", "rozmarný" atd.):'
    ],
    forms: {
      'mladý': [
        's 1  ý$',
        's 1 ější [bdntpv]ý$ 1 ejší [fghklszm]ý$ 2 řejší rý$',
        's 1 í [bflmpsvzkrdtn]ý$ 2 zí [^c]hý$ 1 í chý$'
      ],
      'jarní': [
        's 1 í í$',
        's 1 ější [bmpvn]í$ 1 ejší [žščřcjďťňflsz]í$'
      ]
    },
    patterns: {
      'mladý': [
        '/1ý',
        '/1ého',
        '/1ému',
        '/1ého',
        '/1ý',
        '/1ém',
        '/1ým',
        '/1ý',
        '/1ého',
        '/1ému',
        '/1ý',
        '/1ý',
        '/1ém',
        '/1ým',
        '/1á',
        '/1é',
        '/1é',
        '/1ou',
        '/1á',
        '/1é',
        '/1ou', // instrumental, feminine, singular, positive
        '/1é',
        '/1ého',
        '/1ému',
        '/1é',
        '/1é',
        '/1ém',
        '/1ým',
        '/3',
        '/1ých',
        '/1ým',
        '/1é',
        '/3',
        '/1ých',
        '/1ými',
        '/1é',
        '/1ých',
        '/1ým',
        '/1é',
        '/1é',
        '/1ých',
        '/1ými', // instrumental, inanimate masculine, plural, positive
        '/1é',
        '/1ých',
        '/1ým',
        '/1é',
        '/1é',
        '/1ých',
        '/1ými',
        '/1á',
        '/1ých',
        '/1ým',
        '/1á',
        '/1á',
        '/1ých',
        '/1ými',
        '/2', // nominative, animate masculine, singular, comparative
        '/2ho',
        '/2mu',
        '/2ho',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2', // instrumental, feminine, singular, comparative
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi', // instrumental, animate masculine, plural, comparative
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        'nej/2', // nominative, animate masculine, singular, superlative
        'nej/2ho',
        'nej/2mu',
        'nej/2ho',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2', // instrumental, feminine, singular, superlative
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi', // instrumental, animate masculine, plural, superlative
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi'
      ],
      'jarní': [
        '/1',
        '/1ho',
        '/1mu',
        '/1ho',
        '/1',
        '/1m',
        '/1m',
        '/1',
        '/1ho',
        '/1mu',
        '/1',
        '/1',
        '/1m',
        '/1m',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1',
        '/1ho',
        '/1mu',
        '/1',
        '/1',
        '/1m',
        '/1m',
        '/1', // first plural
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi',
        '/1',
        '/1ch',
        '/1m',
        '/1',
        '/1',
        '/1ch',
        '/1mi', //last plural
        '/2',
        '/2ho',
        '/2mu',
        '/2ho',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2',
        '/2ho',
        '/2mu',
        '/2',
        '/2',
        '/2m',
        '/2m', // last singular comparative
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi',
        '/2',
        '/2ch',
        '/2m',
        '/2',
        '/2',
        '/2ch',
        '/2mi', // last plural comparative
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2ho',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2',
        'nej/2ho',
        'nej/2mu',
        'nej/2',
        'nej/2',
        'nej/2m',
        'nej/2m', // last singular superlative
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi',
        'nej/2',
        'nej/2ch',
        'nej/2m',
        'nej/2',
        'nej/2',
        'nej/2ch',
        'nej/2mi', // last plural superlative
      ]
    }
  },
  'czech-noun-masculine-inanimate': {
    label: 'česká podstatná jména mužská neživotná',
    input_fields: [
      'Zadejte 1. pád čísla jednotného:'
    ],
    forms: {
      'aktivismus': [
        's 2  smus$'
      ],
      'stroj': [
        's 0  $',
        's 0  [^c]$ 2 c c$'
      ]
    },
    patterns: {
      'aktivismus': [
        '/1us',
        '/1u',
        '/1u',
        '/1us',
        '/1e',
        '/1u',
        '/1em',
        '/1y',
        '/1ů',
        '/1ům',
        '/1y',
        '/1y',
        '/1ech',
        '/1y'
      ],
      'stroj': [
        '/1',
        '/2e',
        '/2i',
        '/1',
        '/2i',
        '/2i',
        '/2em',
        '/2e',
        '/2ů',
        '/2ům',
        '/2e',
        '/2e',
        '/2ích',
        '/2i'
      ]
    }
  }
};

new GeneratorWizard(
  document.getElementById('wizard'),
  generators,
  'Zvolte si slovní druh:',
  'Zvolte si vzor skloňování slov:',
  'Generovat',
).init();
